local pcg_utils = {}

local pl = require 'pl.import_into'()

function pcg_utils.countNeighbors(x, y, lvl, tile)
    --- How many of the eight neighbors of this space are of type tile
    --- Counts off-level spaces as matching

    local neighbors = 0
    local t = nil

    if y == 1 then
        neighbors = neighbors + 3
    else
        t = lvl.tiles[y-1][x-1]
        if (t == tile) or (t == nil) then
            neighbors = neighbors + 1
        end

        t = lvl.tiles[y-1][x  ]
        if (t == tile) or (t == nil) then
            neighbors = neighbors + 1
        end

        t = lvl.tiles[y-1][x+1]
        if (t == tile) or (t == nil) then
            neighbors = neighbors + 1
        end
    end

    t = lvl.tiles[y][x-1]
    if (t == tile) or (t == nil) then
        neighbors = neighbors + 1
    end

    t = lvl.tiles[y][x+1]
    if (t == tile) or (t == nil) then
        neighbors = neighbors + 1
    end


    if (y == lvl.height) then
        neighbors = neighbors + 3
    else
        t = lvl.tiles[y+1][x-1]
        if (t == tile) or (t == nil) then
            neighbors = neighbors + 1
        end

        t = lvl.tiles[y+1][x]
        if (t == tile) or (t == nil) then
            neighbors = neighbors + 1
        end

        t = lvl.tiles[y+1][x+1]
        if (t == tile) or (t == nil) then
            neighbors = neighbors + 1
        end
    end

    return neighbors
end


function pcg_utils.smoothLevel(lvl)
    local smoothedLevel = pl.tablex.deepcopy(lvl)
    
    local filledNeighbors = 0

    for y=1,lvl.height do
        for x=1,lvl.width do
            filledNeighbors = pcg_utils.countNeighbors(x, y, lvl, '#')
            if (lvl.tiles[y][x] == '#') and (filledNeighbors == 4) then
                -- stay as a wall
                smoothedLevel.tiles[y][x] = '#'
            elseif (filledNeighbors >= 5) then
                -- become a wall
                smoothedLevel.tiles[y][x] = '#'
            else
                smoothedLevel.tiles[y][x] = '.'
            end
        end
    end

    return smoothedLevel
end

local function fillLevel(lvl, percentSolid)
    for y=1,lvl.height do
        lvl.tiles[y] = {}
        for x=1,lvl.width do
            if math.random() < percentSolid then
                lvl.tiles[y][x] = '#' -- 45% solid
            else
                lvl.tiles[y][x] = '.' -- otherwise be empty
            end
        end
    end
end

-- ensure a border is solid
local function solidifyBorder(lvl)
    for x=1,lvl.width do
        lvl.tiles[1][x] = '#'
        lvl.tiles[lvl.height][x] = '#'
    end

    for y=1,lvl.height do
        lvl.tiles[y][1] = '#'
        lvl.tiles[y][lvl.width] = '#'
    end
end

-- determine which spaces are passable
local function setPassable(lvl)
    for y=1,lvl.height do
        lvl.actions[y] = {}
        for x=1,lvl.width do
            if lvl.tiles[y][x] == '.' then
                lvl.actions[y][x] = 0
            else
                lvl.actions[y][x] = false -- type(false) ~= number
            end
        end
    end
end

function pcg_utils.generateLevel(lvl, percentSolid, smoothingPasses, floorTileDefault, solidTileDefault, addLevelContent)
	fillLevel(lvl, percentSolid)

    if (type(smoothingPasses) == 'number') and (smoothingPasses > 0) then
        for r=1,smoothingPasses do
            lvl = pcg_utils.smoothLevel(lvl)
        end
    end

	solidifyBorder(lvl)
	setPassable(lvl)

    -- convert empty tiles to grass and solid tiles to water with proper shore borders
    local fancyLevel = pl.tablex.deepcopy(lvl)
    for y=1,lvl.height do
        for x=1,lvl.width do
            if lvl.tiles[y][x] == '.' then
                fancyLevel.tiles[y][x] = floorTileDefault -- 14 -- grass tile
            elseif lvl.tiles[y][x] == '#' then
                local landMask = 0
                if y > 1 then
                    if lvl.tiles[y-1][x-1] == '.' then landMask = bit.bor(landMask, 0x80) end
                    if lvl.tiles[y-1][x  ] == '.' then landMask = bit.bor(landMask, 0x40) end
                    if lvl.tiles[y-1][x+1] == '.' then landMask = bit.bor(landMask, 0x20) end
                end

                if lvl.tiles[y  ][x-1] == '.' then landMask = bit.bor(landMask, 0x10) end
                if lvl.tiles[y  ][x+1] == '.' then landMask = bit.bor(landMask, 0x08) end
                
                if y < lvl.height then
                    if lvl.tiles[y+1][x-1] == '.' then landMask = bit.bor(landMask, 0x04) end
                    if lvl.tiles[y+1][x  ] == '.' then landMask = bit.bor(landMask, 0x02) end
                    if lvl.tiles[y+1][x+1] == '.' then landMask = bit.bor(landMask, 0x01) end
                end

                --if type(waterBorder[landMask]) == "number" then
                --    fancyLevel.tiles[y][x] = waterBorder[landMask]
                --else
                    fancyLevel.tiles[y][x] = solidTileDefault -- 0 -- solid water
                --end
            end
        end
    end

    if addLevelContent ~= nil then addLevelContent(fancyLevel) end

    return fancyLevel
end

return pcg_utils

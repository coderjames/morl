local levelData =
{
	width = 70,
	height = 30,
	tiles = {}
}

local function drawLevel(lvl)
	for y=1,lvl.height do
		for x=1,lvl.width do
			io.write(lvl.tiles[y][x])
		end
		io.write('\n')
	end
end

local function countNeighbors(x, y, lvl, tile)
	--- How many of the eight neighbors of this space are of type tile
	--- Counts off-level spaces as matching

	local neighbors = 0
	local t = nil

	if y == 1 then
		neighbors = neighbors + 3
	else
		t = lvl.tiles[y-1][x-1]
		if (t == tile) or (t == nil) then
			neighbors = neighbors + 1
		end

		t = lvl.tiles[y-1][x  ]
		if (t == tile) or (t == nil) then
			neighbors = neighbors + 1
		end

		t = lvl.tiles[y-1][x+1]
		if (t == tile) or (t == nil) then
			neighbors = neighbors + 1
		end
	end

	t = lvl.tiles[y][x-1]
	if (t == tile) or (t == nil) then
		neighbors = neighbors + 1
	end

	t = lvl.tiles[y][x+1]
	if (t == tile) or (t == nil) then
		neighbors = neighbors + 1
	end

	
	if (y == lvl.height) then
		neighbors = neighbors + 3
	else
		t = lvl.tiles[y+1][x-1]
		if (t == tile) or (t == nil) then
			neighbors = neighbors + 1
		end

		t = lvl.tiles[y+1][x]
		if (t == tile) or (t == nil) then
			neighbors = neighbors + 1
		end
		
		t = lvl.tiles[y+1][x+1]
		if (t == tile) or (t == nil) then
			neighbors = neighbors + 1
		end
	end

	return neighbors
end

local function smoothLevel(lvl)
	local smoothedLevel = { width = lvl.width, height=lvl.height, tiles={} }
	local filledNeighbors = 0


	for y=1,lvl.height do
		smoothedLevel.tiles[y] = {}
		for x=1,lvl.width do
			filledNeighbors = countNeighbors(x, y, lvl, '#')
			io.write()
			if (lvl.tiles[y][x] == '#') and (filledNeighbors == 4) then
				-- stay as a wall
				smoothedLevel.tiles[y][x] = '#'
			elseif (filledNeighbors >= 5) then
				-- become a wall
				smoothedLevel.tiles[y][x] = '#'
			else
				smoothedLevel.tiles[y][x] = '.'
			end
		end
	end

	return smoothedLevel
end

local function generateLevel(lvl)
	for y=1,lvl.height do
		lvl.tiles[y] = {}
		for x=1,lvl.width do
			if math.random() < 0.45 then
				lvl.tiles[y][x] = '#' -- with a 45% chance of being a wall
			else
				lvl.tiles[y][x] = '.' -- otherwise be empty
			end
		end
	end

	for r=1,5 do
		lvl = smoothLevel(lvl)
	end

	-- ensure border is solid
	for x=1,lvl.width do
		lvl.tiles[1][x] = '#'
		lvl.tiles[lvl.height][x] = '#'
	end

	for y=1,lvl.height do
		lvl.tiles[y][1] = '#'
		lvl.tiles[y][lvl.width] = '#'
	end

	return lvl
end


math.randomseed(os.time())
levelData = generateLevel(levelData)

io.write('\n\nResulting Level:\n')
drawLevel(levelData)

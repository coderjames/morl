local pl         = require 'pl.import_into'()
local gamera     = require 'gamera'
local anim8      = require 'anim8'
local bit        = require("bit")
local gamestate  = require 'hump.gamestate'

-- morl functions extracted from main are imported into local
-- function names to avoid reworking the calling code
local drawPopupWindow = require 'drawPopup'.drawPopupWindow
local countNeighbors  = require 'pcg_utils'.countNeighbors
local smoothLevel     = require 'pcg_utils'.smoothLevel
local generateLevel   = require 'pcg_utils'.generateLevel

-- scale all tiles by this amount when rendering
-- (e.g. import 16x16 tiles but draw as 32x32)
local TILE_SCALE_FACTOR = 2.0

-- number of seconds that must pass with key held down for player action
-- to be registered
local MOVE_RATE = 0.2

-- percent chance that player will have a monster encounter (battle)
-- when entering an otherwise empty tile
local MONSTER_ENCOUNTER_PERCENTAGE = 10

-- actions
local NO_ACTION = 0
local POP_LEVEL_STACK = 1

-- number of towns in the overworld
local NUMBER_OF_TOWNS = 3
local TOWN_ACTION_OFFSET = 100
local OVERWORLD_SMOOTHING_PASSES = 5

-- number of caves in the overworld
local NUMBER_OF_CAVES = 2
local CAVE_ACTION_OFFSET = 200
local CAVE_WALL_LIGHT_TILE_ID = (6 * 30) + 22
local CAVE_WALL_DARK_TILE_ID = (7 * 30) + 22
local CAVE_FLOOR_COMMON_TILE_ID = (3 * 30) + 22
local CAVE_FLOOR_RARE_TILE_ID = (3 * 30) + 28
local CAVE_FLOOR_RARE_PERCENTAGE = 0.003
local CAVE_SMOOTHING_PASSES = 5

-- number of towers in the overworld
local NUMBER_OF_TOWERS = 2
local TOWER_ACTION_OFFSET = 300
local TOWER_WALL_LIGHT_TILE_ID = (10 * 30) + 15
local TOWER_WALL_DARK_TILE_ID = (11 * 30) + 15
local TOWER_FLOOR_COMMON_TILE_ID = (3 * 30) + 12
local TOWER_FLOOR_RARE_TILE_ID = (2 * 30) + 13
local TOWER_FLOOR_RARE_PERCENTAGE = 0.001
local TOWER_SMOOTHING_PASSES = 5

-- either false when a pop-up is not being displayed, or
-- a function to call in order to draw the active pop-up
local displayPopup = false

-- lookup table to convert from solid water to proper shore borders based
-- on which surrounding tiles are land
-- bit of input mask is SET if tile is land
--
-- 7 6 5
-- 4   3
-- 2 1 0
--
local waterBorder = {
    [0x00] = 0,  -- water all around, so stay solid water

    [0x01] = 53,
    [0x04] = 52,
    [0x20] = 55,
    [0x80] = 54,

    [0x03] = 1,  -- land all across bottom
    [0x06] = 1,  -- land all across bottom
    [0x07] = 1,  -- land all across bottom
    [0x02] = 1,

    [0x60] = 5,  -- land all across top
    [0xc0] = 5,  -- land all across top
    [0xe0] = 5,  -- land all across top
    [0x40] = 5,
    
    [0x09] = 4,  -- land all along right edge
    [0x28] = 4,  -- land all along right edge
    [0x29] = 4,  -- land all along right edge
    [0x08] = 4,
    
    [0x14] = 10, -- land all along left edge
    [0x90] = 10, -- land all along left edge
    [0x94] = 10, -- land all along left edge
    [0x10] = 10,

    [0x0b] = 3,  -- land in bottom-right corner
    [0x0f] = 3,  -- land in bottom-right corner
    [0x2b] = 3,
    [0x1b] = 3,  -- land in bottom-right corner
    [0x2f] = 3,  -- land in bottom-right corner

    [0x16] = 2,  -- land in bottom-left corner
    [0x17] = 2,  -- land in bottom-left corner
    [0x96] = 2,  -- land in bottom-left corner
    [0x97] = 2,  -- land in bottom-left corner

    [0xd0] = 9,  -- land in top-left corner
    [0xd4] = 9,  -- land in top-left corner
    [0xf0] = 9,  -- land in top-left corner
    [0xf4] = 9,  -- land in top-left corner

    [0x68] = 6,  -- land in top-right corner
    [0x69] = 6,  -- land in top-right corner
    [0xe8] = 6,  -- land in top-right corner
    [0xe9] = 6,  -- land in top-right corner

    [0xf8] = 13,
    [0x21] = 59,
    [0xa0] = 126,
    [0x81] = 127,
    [0x24] = 62,
    [0x1f] = 13, -- actually need to vertically flip 13 to be 
                -- right for this position b/c tileset doesn't include
                -- the necessary tile (b/c SNES gfx engine can do flips)
    [0xd6] = 11,
    [0xd7] = 11,
    [0xa7] = 50,
    [0x6f] = 7,
    [0xff] = 17, -- tall mountain
    [0x84] = 59, -- actually need to horizontally flip 59 to be correct
    [0x6b] = 11, -- actually need to horizontally flip 11 to be correct
    [0xfc] = 13,
    [0xb5] = 46, -- actually need to horizontally flip 46 to be correct
    [0x9f] = 13,
    [0x05] = 58


}

local tileData = {
    TileWidth = 16,
    TileHeight = 16    
}

-- points to the active level
local levelData = {}

-- the stack of levels from whence the player came (empty when on starting level)
local levelStack = {}

-- level representing the overworld map
local overworld = {
    width = 70,
    height = 30,
    tiles = {},
    actions = {},
}

-- all the other places (levels) that are not the overworld
local places = {}

local playerData = {
    width=tileData.TileWidth * TILE_SCALE_FACTOR,
    height=tileData.TileHeight * TILE_SCALE_FACTOR,
    verticalMovement = 0.0,
    horizontalMovement = 0.0
}

local function checkNumber(value, name)
  if type(value) ~= 'number' then
    error(name .. " must be a number (was: " .. tostring(value) .. ")")
  end
end

local function drawMonsterEncounterPopup()
  drawPopupWindow(400, 500, 20,
                  {'You have encountered a monster!',
                   'Time to fight for your life!', })
end

local function drawEnterTownPopup()
  drawPopupWindow(400, 500, 20,
                  {'You are entering a town.'})
end

local function placeCaveContents( lvl )
    local x = 0
    local y = 0
    
    -- the 'up' stairs leading out
    repeat
        x = math.random(1,lvl.width)
        y = math.random(1,lvl.height)
    until (lvl.actions[y][x] == 0) -- a passable tile without any existing action
    lvl.entry_x = x
    lvl.entry_y = y
    lvl.actions[y][x] = POP_LEVEL_STACK
    lvl.tiles[y][x] = {lvl.tiles[y][x], (14 * 30) + 25}

    -- make the walls a bit fancier by shading the "bottom" bricks where walls touch floors
    for y=1,lvl.height-1 do
        for x=1,lvl.width do
            -- if this is a wall and the tile "south" or "below" this one is floor, then
            if (lvl.tiles[y][x] == CAVE_WALL_LIGHT_TILE_ID) and
                (lvl.tiles[y+1][x] == CAVE_FLOOR_COMMON_TILE_ID) then
                -- replace the light wall with dark wall to appear "shaded"
                lvl.tiles[y][x] = CAVE_WALL_DARK_TILE_ID
            end
        end
    end

    -- some floor spaces can be "rare" with a non-functional style item added
    -- all borders are fixed as solid walls, so don't bother scanning the edges
    for y=2,lvl.height-1 do
        for x=2,lvl.width-1 do
            if (lvl.tiles[y][x] == CAVE_FLOOR_COMMON_TILE_ID) and
                (love.math.random() < CAVE_FLOOR_RARE_PERCENTAGE) then
                lvl.tiles[y][x] = {CAVE_FLOOR_COMMON_TILE_ID, CAVE_FLOOR_RARE_TILE_ID}
            end
        end
    end
end


local function placeTowerContents( lvl )
    local x = 0
    local y = 0
    
    -- the 'down' stairs leading out
    repeat
        x = math.random(1,lvl.width)
        y = math.random(1,lvl.height)
    until (lvl.actions[y][x] == 0) -- a passable tile without any existing action
    lvl.entry_x = x
    lvl.entry_y = y
    lvl.actions[y][x] = POP_LEVEL_STACK
    lvl.tiles[y][x] = {lvl.tiles[y][x], (15 * 30) + 25}

    -- make the walls a bit fancier by shading the "bottom" bricks where walls touch floors
    for y=1,lvl.height-1 do
        for x=1,lvl.width do
            -- if this is a wall and the tile "south" or "below" this one is floor, then
            if (lvl.tiles[y][x] == TOWER_WALL_LIGHT_TILE_ID) and
                (lvl.tiles[y+1][x] == TOWER_FLOOR_COMMON_TILE_ID) then
                -- replace the light wall with dark wall to appear "shaded"
                lvl.tiles[y][x] = TOWER_WALL_DARK_TILE_ID
            end
        end
    end

    -- some floor spaces can be "rare" holes which act as down stairs
    -- all borders are fixed as solid walls, so don't bother scanning the edges
    for y=2,lvl.height-1 do
        for x=2,lvl.width-1 do
            if (lvl.tiles[y][x] == TOWER_FLOOR_COMMON_TILE_ID) and
                (love.math.random() < TOWER_FLOOR_RARE_PERCENTAGE) then
                lvl.tiles[y][x] = TOWER_FLOOR_RARE_TILE_ID
                lvl.actions[y][x] = POP_LEVEL_STACK
            end
        end
    end
end

local function loadLevel(filename)
    local lvl = {}
    local ok, chunk, result
    
    ok, chunk = pcall( love.filesystem.load, filename ) -- load the chunk safely
    if not ok then
      print('The following error happend: ' .. tostring(chunk) .. ' while loading ' .. filename)
    else
      ok, result = pcall(chunk) -- execute the chunk safely
     
      if not ok then -- will be false if there is an error
        print('The following error happened: ' .. tostring(result) .. ' while loading ' .. filename)
      else
        lvl = result
      end
    end

    return lvl
end

local function placeOverworldStructures(fancyLevel)
    local placeIndex = 0

    -- add a few towns
    for t=1,NUMBER_OF_TOWNS do
        local x = 0
        local y = 0
        repeat
            x = math.random(1,fancyLevel.width)
            y = math.random(1,fancyLevel.height)
        until (fancyLevel.actions[y][x] == 0)
        
        local p = math.random()
        if p < 0.33 then
            fancyLevel.tiles[y][x  ] = {fancyLevel.tiles[y][x], (9 * 30) + 21}
        elseif p < 0.66 then
            fancyLevel.tiles[y][x  ] = {fancyLevel.tiles[y][x], (8 * 30) + 22}
        else
            fancyLevel.tiles[y][x] = {fancyLevel.tiles[y][x], (9 * 30) + 22}         
        end

        placeIndex = TOWN_ACTION_OFFSET+t

        fancyLevel.actions[y][x  ] = placeIndex -- record which town is entered when player occupies this square
    
        --places[placeIndex] = loadLevel(string.format('town%d.lua', t))
        places[placeIndex] = loadLevel(string.format('town1.lua'))
    end

    -- add a couple caves
    for c=1,NUMBER_OF_CAVES do
        local x = 0
        local y = 0
        repeat
            x = math.random(1,fancyLevel.width)
            y = math.random(1,fancyLevel.height)
        until (fancyLevel.actions[y][x] == 0) -- a passable tile without any existing action

        placeIndex = CAVE_ACTION_OFFSET+c

        -- add the cave tile on top of whatever is already there
        fancyLevel.tiles[y][x] = {fancyLevel.tiles[y][x], (13 * 30) + 23}
        fancyLevel.actions[y][x] = placeIndex -- record which place is entered when player occupies this square

        places[placeIndex] = { width = 50, height = 20, tiles = {}, actions = {} }
        places[placeIndex] = generateLevel(places[placeIndex], 0.45, CAVE_SMOOTHING_PASSES, CAVE_FLOOR_COMMON_TILE_ID, CAVE_WALL_LIGHT_TILE_ID, placeCaveContents)
    end

    -- add a couple buildings
    for c=1,NUMBER_OF_TOWERS do
        local x = 0
        local y = 0
        repeat
            x = math.random(2,fancyLevel.width-1)
            y = math.random(2,fancyLevel.height-1)
        until (fancyLevel.actions[y][x] == 0) -- a passable tile without any existing action

        local p = math.random()
        if p < 0.5 then -- broken tower
            fancyLevel.tiles[y-1][x  ] = {fancyLevel.tiles[y-1][x], (12 * 30) + 21}
            fancyLevel.tiles[y  ][x  ] = {fancyLevel.tiles[y  ][x], (13 * 30) + 21}
        else -- complete tower
            fancyLevel.tiles[y-1][x  ] = {fancyLevel.tiles[y-1][x], (12 * 30) + 20}
            fancyLevel.tiles[y  ][x  ] = {fancyLevel.tiles[y  ][x], (13 * 30) + 20}
        end

        placeIndex = TOWER_ACTION_OFFSET+c
        fancyLevel.actions[y][x  ] = placeIndex -- record which place is entered when player occupies this square

        places[placeIndex] = { width = 30, height = 40, tiles = {}, actions = {} }
        places[placeIndex] = generateLevel(places[placeIndex], 0.40, TOWER_SMOOTHING_PASSES, TOWER_FLOOR_COMMON_TILE_ID, TOWER_WALL_LIGHT_TILE_ID, placeTowerContents)
    end
end

function love.load()
    love.graphics.setDefaultFilter( 'nearest', 'nearest' )
    guiFont = love.graphics.newFont("KenneyPixel.ttf", 32)
    titleFont = love.graphics.newFont("KenneyBold.ttf", 32)
    
    tileData.tileAtlas = love.graphics.newImage( "RPG32x32.png" )
    tileData.tiles = {}
    local imgWidth, imgHeight = tileData.tileAtlas:getDimensions()
    local VerticalTileCount = imgHeight / tileData.TileHeight
    local HorizontalTileCount = imgWidth / tileData.TileWidth
    for y = 0,VerticalTileCount do
        for x = 0,HorizontalTileCount do
            tileData.tiles[y*HorizontalTileCount+x] =
                    love.graphics.newQuad(x * tileData.TileWidth  * TILE_SCALE_FACTOR,
                                          y * tileData.TileHeight * TILE_SCALE_FACTOR,
                                          tileData.TileWidth  * TILE_SCALE_FACTOR,
                                          tileData.TileHeight * TILE_SCALE_FACTOR,
                                          imgWidth * TILE_SCALE_FACTOR,
                                          imgHeight * TILE_SCALE_FACTOR)
        end
    end
    monsterAtlas = love.graphics.newImage("dq1_snes_monsters.png")
    
    characterAtlas = love.graphics.newImage("dq1_snes_characters.png")
    characterTiles = {}
    local imgWidth, imgHeight = characterAtlas:getDimensions()
    local VerticalTileCount = imgHeight / tileData.TileHeight
    local HorizontalTileCount = imgWidth / tileData.TileWidth
    for y = 0,VerticalTileCount do
        for x = 0,HorizontalTileCount do
            characterTiles[y*HorizontalTileCount+x] =
                    love.graphics.newQuad(x * tileData.TileWidth  * TILE_SCALE_FACTOR,
                                          y * tileData.TileHeight * TILE_SCALE_FACTOR,
                                          tileData.TileWidth  * TILE_SCALE_FACTOR,
                                          tileData.TileHeight * TILE_SCALE_FACTOR,
                                          imgWidth * TILE_SCALE_FACTOR,
                                          imgHeight * TILE_SCALE_FACTOR)
        end
    end


    math.randomseed(os.time())
    overworld = generateLevel(overworld, 0.45, OVERWORLD_SMOOTHING_PASSES, (10 * 30 ) + 1, (4 * 30) + 1, placeOverworldStructures)

    levelData = overworld

    camera = gamera.new(0,0,
                    levelData.width * tileData.TileWidth * TILE_SCALE_FACTOR,
                    levelData.height * tileData.TileHeight * TILE_SCALE_FACTOR)
                    
    -- pick a random starting spot for the player, but require it to be empty
    playerData.x = math.random(1, levelData.width)
    playerData.y = math.random(1, levelData.height)
    while levelData.actions[playerData.y][playerData.x] ~= NO_ACTION do
        playerData.x = math.random(1, levelData.width)
        playerData.y = math.random(1, levelData.height)        
    end

    playerData.facing = "down"
    playerData.animation = {}
    playerData.animation["left" ] = anim8.newAnimation({characterTiles[6], characterTiles[7]}, 0.2)
    playerData.animation["right"] = anim8.newAnimation({characterTiles[4], characterTiles[5]}, 0.2)
    playerData.animation["up"   ] = anim8.newAnimation({characterTiles[2], characterTiles[3]}, 0.2)
    playerData.animation["down" ] = anim8.newAnimation({characterTiles[0], characterTiles[1]}, 0.2)
    
    camera:setPosition(playerData.x * tileData.TileWidth * TILE_SCALE_FACTOR,
                       playerData.y * tileData.TileWidth * TILE_SCALE_FACTOR)


    title_music = love.audio.newSource("title_music.mp3", "stream")

    overworld_background_music = love.audio.newSource("overworld_wandering_music.wav", "stream")
    overworld_background_music:setLooping(true)
    --overworld_background_music:play()
end

local function processInputs( dt )
    if love.keyboard.isDown("escape") then
        love.event.quit()
    end

    local enteredNewTile = false

    if love.keyboard.isDown("up") then
        playerData.verticalMovement = playerData.verticalMovement - dt
        playerData.facing = "up"
    elseif love.keyboard.isDown("down") then
        playerData.verticalMovement = playerData.verticalMovement + dt
        playerData.facing = "down"
    else
        playerData.verticalMovement = 0.0
    end

    if love.keyboard.isDown("left") then
        playerData.horizontalMovement = playerData.horizontalMovement - dt
        playerData.facing = "left"
    elseif love.keyboard.isDown("right") then
        playerData.horizontalMovement = playerData.horizontalMovement + dt
        playerData.facing = "right"
    else
        playerData.horizontalMovement = 0.0
    end

    if playerData.verticalMovement < -MOVE_RATE then
        playerData.verticalMovement = 0.0
        if type(levelData.actions[playerData.y - 1][playerData.x]) == "number" then
            playerData.y = playerData.y - 1
            enteredNewTile = true
        end
    elseif playerData.verticalMovement > MOVE_RATE then
        playerData.verticalMovement = 0.0
        if type(levelData.actions[playerData.y + 1][playerData.x]) == "number" then
            playerData.y = playerData.y + 1
            enteredNewTile = true
        end
    end

    if playerData.horizontalMovement < -MOVE_RATE then
        playerData.horizontalMovement = 0.0
        if type(levelData.actions[playerData.y][playerData.x - 1]) == "number" then
            playerData.x = playerData.x - 1
            enteredNewTile = true
        end
    elseif playerData.horizontalMovement > MOVE_RATE then
        playerData.horizontalMovement = 0.0
        if type(levelData.actions[playerData.y][playerData.x + 1]) == "number" then
            playerData.x = playerData.x + 1
            enteredNewTile = true
        end
    end

    return enteredNewTile
end

function love.update(dt)
    if displayPopup == false then
        -- do normal game input processing and state updating
        local enteredNewTile = processInputs(dt)

        if enteredNewTile == true then
            local action = levelData.actions[playerData.y][playerData.x]
            if action == NO_ACTION then
                -- not entering a new place, so check for random monster encounter
                local rolledPercentage = math.random(1, 100)
                if rolledPercentage < MONSTER_ENCOUNTER_PERCENTAGE then
                    -- trigger a monster encounter
                    --displayPopup = drawMonsterEncounterPopup
                end
            elseif action == POP_LEVEL_STACK then
                -- if levelStack is not empty then
                levelData,playerData.x,playerData.y = unpack(table.remove(levelStack))
                camera:setWorld(0,0,
                    levelData.width * tileData.TileWidth * TILE_SCALE_FACTOR,
                    levelData.height * tileData.TileHeight * TILE_SCALE_FACTOR)
                -- else
                --    report fatal error: tried to walk off the end of the universe
                -- end
            elseif (type(places[action]) == 'table') then
                -- this is a portal (stairs,entry-point) to a new level
                -- need to:
                --   remember where on the current map player was
                --   remember what the current map was
                --   update levelData to point at places[action]
                --   update playerData to start on 'up' stairs

                checkNumber(places[action].entry_x, string.format("places[%d].entry_x", action))
                checkNumber(places[action].entry_y, string.format("places[%d].entry_y", action))
                checkNumber(places[action].width, string.format("places[%d].width", action))
                checkNumber(places[action].height, string.format("places[%d].height", action))
                
                -- push {levelData,playerData.x,playerData.y} onto levelStack
                table.insert(levelStack, {levelData,playerData.x,playerData.y})
                levelData = places[action]

                playerData.x = levelData.entry_x
                playerData.y = levelData.entry_y

                checkNumber(levelData.width * tileData.TileWidth * TILE_SCALE_FACTOR, "w*TW*TSF")
                checkNumber(levelData.height * tileData.TileHeight * TILE_SCALE_FACTOR, "h*TH*TSF")
                camera:setWorld(0,0,
                    levelData.width * tileData.TileWidth * TILE_SCALE_FACTOR,
                    levelData.height * tileData.TileHeight * TILE_SCALE_FACTOR)
            end
        end

        playerData.animation[playerData.facing]:update(dt)

        camera:setPosition(playerData.x * tileData.TileWidth * TILE_SCALE_FACTOR,
                       playerData.y * tileData.TileWidth * TILE_SCALE_FACTOR)
    elseif type(displayPopup) == 'function' then
        -- some pop-ups also have their own update functions
        if type(updatePopup) == 'function' then
            updatePopup(dt)
        end
    end
end

function love.draw()
    love.graphics.setColor(255, 255, 255) -- reset color to white
    camera:draw(function(visible_left,visible_top,visible_width,visible_height)
        -- consider one more tile visible in each direction so that the partial
        -- tiles at the edges are not clipped out entirely
        visible_left = visible_left - tileData.TileWidth*TILE_SCALE_FACTOR
        visible_top  = visible_top  - tileData.TileHeight*TILE_SCALE_FACTOR

        local visible_right = visible_left + visible_width + 2*tileData.TileWidth*TILE_SCALE_FACTOR
        local visible_bottom = visible_top + visible_height + 2*tileData.TileHeight*TILE_SCALE_FACTOR

        local insideCamera = false
            
        for y=1,levelData.height do
            local tileY = (y-1) * tileData.TileHeight*TILE_SCALE_FACTOR
            for x=1,levelData.width do
                local tileX = (x-1) * tileData.TileWidth*TILE_SCALE_FACTOR
                
                insideCamera = (tileX >= visible_left)  and
                               (tileX <= visible_right) and
                               (tileY >= visible_top)   and
                               (tileY <= visible_bottom)
        
                if insideCamera then
                    -- Each cell of the tiles[][] table is either a number, representing
                    -- the one tile to draw in that position or a list of numbers,
                    -- representing the tiles to draw from back (first) to front (last)
                    -- anything else gets drawn as a solid red square to flag an error
                    if type(levelData.tiles[y][x]) == 'number' then
                        love.graphics.draw(tileData.tileAtlas, tileData.tiles[levelData.tiles[y][x]], tileX, tileY)
                    elseif type(levelData.tiles[y][x]) == 'table' then
                        for _,tileNum in ipairs(levelData.tiles[y][x]) do
                            -- DEBUG: sometimes draw() is getting called with nil instead of
                            --        a valid quad??
                            if tileData.tiles[tileNum] ~= nil then
                                love.graphics.draw(tileData.tileAtlas, tileData.tiles[tileNum], tileX, tileY)
                            else
                                -- log the x,y,tileNum that was nil
                            end
                        end
                    else
                        love.graphics.setColor(250, 0, 0) -- solid red for error (unknown tile)
                        love.graphics.rectangle( "fill", tileX, tileY,
                                                tileData.TileWidth*TILE_SCALE_FACTOR,
                                                tileData.TileHeight*TILE_SCALE_FACTOR)    
                        love.graphics.setColor(255, 255, 255) -- reset color to white
                    end                    
                end
            end
        end 
        
        -- draw the player
        local playerX = (playerData.x - 1) * tileData.TileWidth  * TILE_SCALE_FACTOR
        local playerY = (playerData.y - 1) * tileData.TileHeight * TILE_SCALE_FACTOR
        playerData.animation[playerData.facing]:draw(characterAtlas, playerX, playerY)
        
    end)

    if type(displayPopup) == 'function' then
        displayPopup()
    end
end

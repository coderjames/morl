local drawPopup = {}

function drawPopup.drawPopupWindow(windowWidth, windowHeight, border, message)
    -- compute the necessary coordinates of the top-left corner of the popup window
    -- in order to display the popup centered in the game window
    local gameWindowWidth = love.graphics.getWidth( )
    local rectLeftX = (gameWindowWidth - windowWidth) / 2
    local gameWindowHeight = love.graphics.getHeight( )
    local rectTopY = (gameWindowHeight - windowHeight) / 2

    -- draw the border around the popup
    love.graphics.setColor(255, 255, 255, 255)
    love.graphics.rectangle( "fill", rectLeftX, rectTopY, windowWidth, windowHeight)
  
    -- update the coordinates and dimensions of the window and the to account for
    -- the border once so the rest of the function doesn't have to
    windowWidth = windowWidth - (border * 2)
    rectLeftX = (gameWindowWidth - windowWidth) / 2
    windowHeight = windowHeight - (border * 2)
    rectTopY = (gameWindowHeight - windowHeight) / 2

    -- draw the background of the window to clear it
    love.graphics.setColor(2, 83, 222, 255)
    love.graphics.rectangle( "fill",
                                rectLeftX,
                                rectTopY,
                                windowWidth,
                                windowHeight)

    -- write the lines of the message
    local line_y = rectTopY
    local line_x = rectLeftX + 20
    love.graphics.setColor(255, 255, 255, 255)   
    love.graphics.setFont(guiFont)

    for i,s in ipairs(message) do
        love.graphics.print(s, line_x, line_y + (i * 20))
    end
end

return drawPopup
